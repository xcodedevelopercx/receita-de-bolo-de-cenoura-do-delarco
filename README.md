# Receita de Bolo de Cenoura com Cobertura de Chocolate
BOLO DE CEBOLA MANO!!! 

MASSA:
3 ovos
1 xícara de água
1/2 xícara de óleo de girassol
1 colher de sopa sal
1 colher de sopa de açucar
2 xícaras de farinha de trigo
1 colher de sopa de fermento em pó
COBERTURA:
3 cebolas médias
1 colher de sopa de sal
3 colheres de sopa de nata
Orégano a gosto

![Bolo de Cenoura](bolo_cenoura.jpg)

## 1 - Ingredientes

### 1.1 - Para a massa

* 3 cenouras médias raspadas e picadas
* 1 xícara de óleo
* 2 xícaras de açúcar
* 3 ovos
* 1 colher sopa fermento em pó
* 1 pitada de sal
* 2 xícaras farinha de trigo

### 1.2 - Para a calda (cobertura)

* 1/2 xícara de leite
* 6 colheres de açúcar
* 2 colheres chocolate em pó
* 1 colher manteiga

## 2 - Modo de preparo

### 2.1 - Massa

1. Bata no liquidificador todos os ingredientes da massa menos a farinha de trigo
1. Depois de tudo batido, acrescente a farinha aos poucos e bata um pouco mais até formar bolhas
1. Unte a forma nº 2 e ponha para assar
1. Depois de assado e ainda quente, jogue a cobertura

### 2.2 Cobertura

1. Leve todos os ingredientes ao fogo até engrossar
1. Jogue sobre o bolo quente
